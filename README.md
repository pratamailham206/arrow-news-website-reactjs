<h1 align="center">The Arrow News</h1>

[![React](https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB)](https://reactjs.org/)

The Arrow News is news article website, user can read posted article or user can create new post. create new category and manage their post. user also can add like and comment to another post.

<img src="public/static/ss/1.PNG"  width="33%">
<img src="public/static/ss/6.PNG"  width="33%">
<img src="public/static/ss/7.PNG"  width="33%">

#### Features

- Authentication User: Login, register
- See Post
- Create new Post
- Add Like and Comment
- Search Post
- Filter Post
- Create Category

#### Platform

Web

### Project status

This project published in [The Arrow News](https://the-arrow-news.netlify.app/)

# Getting started

Install

### `npm run start`

Start project.

### `npm run start`

Build

### `npm run build`

# What's included

- Introduction Page
- Authentication User
- CRUD data integrated with custom API
- Create new post
- Update profile
- Create new accout
- Like, Comment system

### Custom API

this project integrated with [Custom API](https://github.com/IlhamPratama1/InteractiveLinkedList-API-NodeJs) using Express Node Js 

# Authors

* Ilham Pratama - [Gitlab](https://gitlab.com/pratamailham206)

# Screenshot

<img src="public/static/ss/1.PNG"  width="33%">
<img src="public/static/ss/2.PNG"  width="33%">
<img src="public/static/ss/3.PNG"  width="33%">
<img src="public/static/ss/4.PNG"  width="33%">
<img src="public/static/ss/5.PNG"  width="33%">
<img src="public/static/ss/6.PNG"  width="33%">
<img src="public/static/ss/7.PNG"  width="33%">

# License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
